/*
 * To change this license header, choose License Headers in Project Properties.
 */
package ia2;

import java.awt.*;

/**
 *
 * @author Juan Cid <jucid@alumnos.ubiobio.cl>
 */
public class mensaje extends celda implements ctes {
    int alto,ancho;
    String mString;
    public mensaje(int i, int j, int tipo, Image icon, ia2.ciudad ciudad) {
        super(ciudad.celdas[i][j].x, ciudad.celdas[i][j].y, i, j, tipo, icon, ciudad);
        this.ancho=500;
        this.alto=100;
        mString="Cartero en el Portal";
    }

    @Override
    public void update(Graphics g) {
        if (ciudad.el_canvas.move.msg == true) {
            g.setColor(Color.darkGray);
            g.fillRect(x, y, ancho, alto);
            g.setColor(Color.yellow);
            g.drawRect(x, y, ancho, alto);
            g.setColor(Color.green);
            g.setFont(new Font("Serif", Font.PLAIN, 40));
            g.drawString(mString, x + 5, y + 35);
            g.setColor(Color.blue);
            g.drawString("Haga clic para continuar", x + 5, y + 75);
        }
    }

    @Override
    public void cambiar_celda(int clickX, int clickY) {

    }

    public boolean cerrar(int clickX, int clickY) {
        Rectangle rectanguloCelda = new Rectangle(x, y, ancho, alto);
        return rectanguloCelda.contains(new Point(clickX, clickY));
    }
}
