/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia2;

/**
 *
 * @author Juan
 */
public class estado {
    
    public int x;
    public int y;
    public char oper; 
    public estado predecesor;
    
    public estado(int x, int y, char oper,estado predecesor) {
        this.x=x;
        this.y=y;
        this.oper=oper;
        this.predecesor=predecesor;
    }
    
    @Override
    public boolean equals(Object x) {
        estado e=(estado)x;
        return this.x==e.x && this.y==e.y;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + this.x;
        hash = 89 * hash + this.y;
        return hash;
    }
        
    @Override
    public String toString() {
        return "("+x+","+y+")";
    }
}
