/*
 * To change this license header, choose License Headers in Project Properties.
 */
package ia2;

import java.awt.Graphics;
import java.awt.Image;

/**
 *
 * @author Juan Cid <jucid@alumnos.ubiobio.cl>
 */
public class bus extends celda implements ctes {

    int pasajeros,tipoAnterior;
    Image imagePasajero, iconBack, iconGo;
    char sentido;
    boolean changed = false;

    public bus(int i, int j, int tipo, Image icon, ciudad ciudad, Image carta, int pasajeros, Image iconBack) {
        super(ciudad.celdas[i][j].x, ciudad.celdas[i][j].y, i, j, tipo, icon, ciudad);
        this.imagePasajero = carta;
        this.iconGo = icon;
        this.iconBack = iconBack;
        this.sentido = 'l';
        this.pasajeros = pasajeros;
        this.tipoAnterior = ciudad.celdas[i][j].tipo;
        ciudad.celdas[i][j].tipo = tipo;
    }

    @Override
    public void update(Graphics g) {
        g.drawImage(icon, x, y, this);
        int suma = DIM_CELDA + 1;
        switch (sentido) {
            case 'd':
                for (int k = 1; k < pasajeros + 1; k++) {
                    g.drawImage(imagePasajero, x, y - k * suma, this);
                }
                break;
            case 'u':
                for (int k = 1; k < pasajeros + 1; k++) {
                    g.drawImage(imagePasajero, x, y + k * suma, this);
                }
                break;
            case 'l':
                for (int k = 1; k < pasajeros + 1; k++) {
                    g.drawImage(imagePasajero, x + k * suma, y, this);
                }
                break;
            case 'r':
                for (int k = 1; k < pasajeros + 1; k++) {
                    g.drawImage(imagePasajero, x - k * (DIM_CELDA + 1), y, this);
                }
                break;
        }

//        g.setColor(Color.red);
//        Font fuente = new Font("Monospaced", Font.PLAIN, 10);
//        g.setFont(fuente);
//        g.drawString(i + "," + j, x + 1, y + 25);
    }

    protected void arriba() {
        if (j > 0) {
            if (puedo_pizar(0, -1)) {
                avanzar('u');
                this.sentido = 'u';
            }
        }
    }

    protected void abajo() {
        if (j + 1 < CELDAS_ALTO) {
            if (puedo_pizar(0, 1)) {
                avanzar('d');
                this.sentido = 'd';
            }
        }
    }

    public void izq() {
        if (changed) {
            this.icon = iconGo;
            changed = false;
        }
        if (i > 0) {
            if (puedo_pizar(-1, 0)) {
                avanzar('l');
                this.sentido = 'l';
            }
        }
    }

    protected void der() {
        if (!changed) {
            this.icon = iconBack;
            changed = true;
        }
        if (i + 1 < CELDAS_ANCHO) {
            if (puedo_pizar(1, 0)) {
                avanzar('r');
                this.sentido = 'r';
            }
        }
    }

    public boolean puedo_pizar(int x, int y) {
        return ciudad.celdas[i + x][j + y].tipo == 1 || ciudad.celdas[i + x][j + y].tipo == 7
                || ciudad.celdas[i + x][j + y].tipo == 8;
    }

    public void avanzar(char mov) {
        ciudad.celdas[i][j].tipo = tipoAnterior;
        int suma = DIM_CELDA + 1;
        switch (mov) {
            case 'd':
                j++;
                y += suma;
                break;
            case 'u':
                j--;
                y -= suma;
                break;
            case 'l':
                i--;
                x -= suma;
                break;
            case 'r':
                i++;
                x += suma;
                break;
        }
        this.tipoAnterior = ciudad.celdas[i][j].tipo;
        ciudad.celdas[i][j].tipo=tipo;
    }
}
