/*
 * To change this license header, choose License Headers in Project Properties.
 */
package ia2;

/**
 *
 * @author Juan Cid <jucid@alumnos.ubiobio.cl>
 */
public interface ctes {
    public final int DIM_CELDA = 40;

    public final int CELDAS_ANCHO = 46;
    public final int CELDAS_ALTO = 21;

    public final int ANCHO_V = DIM_CELDA * CELDAS_ANCHO;
    public final int ALTO_V = DIM_CELDA * CELDAS_ALTO;

    public final int CANT_SPRITES = 20;
    
    public final int CANT_CARTAS = 7;
    public final int CANT_PASAJEROS = 9;
    public final int CANT_PEATONES = 6;
    
     public final int CANT_AUTOS = 11;
     
     public final int CANT_PEATONES1 = 40;
     public final int CANT_PEATONES2 = 4;
     public final int CANT_PEATONES3 = 24;
     public final int CANT_PEATONES4 = 64;
     public final int CANT_PEATONES5 = 41;
     public final int CANT_PEATONES6 = 84;
     public final int CANT_PEATONES7 = 14;
     public final int CANT_PEATONES8 = 24;
     
}
