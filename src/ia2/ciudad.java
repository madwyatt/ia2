/*
 * To change this license header, choose License Headers in Project Properties.
 */
package ia2;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.StringTokenizer;
import javax.swing.ImageIcon;
import javax.swing.JComponent;

/**
 *
 * @author Juan Cid <jucid@alumnos.ubiobio.cl>
 */
public class ciudad extends JComponent implements ctes {

    public celda[][] celdas;
    public mi_canvas el_canvas;
    public ImageIcon[] icons = new ImageIcon[CANT_SPRITES];
    public cartero cartero;
    public bus bus;
    public auto[] autos;
    public peaton[] peatones;
    mensaje mensaje;
    celdaNum[] nums;
    ladron ladron;

    public ciudad(mi_canvas canvas_padre) {
        this.el_canvas = canvas_padre;
        celdas = new celda[CELDAS_ANCHO][CELDAS_ALTO];

        cargarImages();
        for (StringTokenizer stringTokenizer = new StringTokenizer(leer("include\\celdas.txt")); stringTokenizer.hasMoreTokens();) {
            for (int j = 0; j < CELDAS_ALTO; j++) {
                for (int i = 0; i < CELDAS_ANCHO; i++) {
                    int temp = Integer.parseInt(stringTokenizer.nextToken());
                    celdas[i][j] = new celda(i + (i * DIM_CELDA), j + (j * DIM_CELDA), i, j, temp, icons[temp].getImage(), this);
                    if (celdas[i][j].tipo == 0 && i >= 2 && j >= 3 && i <= 10 && j <= 9) {//2,3
                        celdas[i][j].cant_peatones = CANT_PEATONES1;
                    } else {
                        if (celdas[i][j].tipo == 0 && i >= 13 && j >= 3 && i <= 21 && j <= 9) {//2,3
                            celdas[i][j].cant_peatones = CANT_PEATONES2;
                        } else {
                            if (celdas[i][j].tipo == 0 && i >= 24 && j >= 3 && i <= 32 && j <= 9) {//2,3
                                celdas[i][j].cant_peatones = CANT_PEATONES3;
                            } else {
                                if (celdas[i][j].tipo == 0 && i >= 35 && j >= 3 && i <= 43 && j <= 9) {//2,3
                                    celdas[i][j].cant_peatones = CANT_PEATONES4;
                                } else {
                                    if (celdas[i][j].tipo == 0 && i >= 2 && j >= 12 && i <= 10 && j <= 18) {//2,3
                                        celdas[i][j].cant_peatones = CANT_PEATONES5;
                                    } else {
                                        if (celdas[i][j].tipo == 0 && i >= 2 && j >= 12 && i <= 10 && j <= 18) {//2,3
                                            celdas[i][j].cant_peatones = CANT_PEATONES6;
                                        } else {
                                            if (celdas[i][j].tipo == 0 && i >= 2 && j >= 12 && i <= 10 && j <= 18) {//2,3
                                                celdas[i][j].cant_peatones = CANT_PEATONES7;
                                            } else {
                                                if (celdas[i][j].tipo == 0 && i >= 2 && j >= 12 && i <= 10 && j <= 18) {//2,3
                                                    celdas[i][j].cant_peatones = CANT_PEATONES8;
                                                } else {
                                                    celdas[i][j].cant_peatones = 50;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        nums = new celdaNum[8];
        nums[0]=new celdaNum(6, 6, 0, icons[0].getImage(), this,CANT_PEATONES1);
        nums[1]=new celdaNum(17, 6, 0, icons[0].getImage(), this,CANT_PEATONES2);
        nums[2]=new celdaNum(28, 6, 0, icons[0].getImage(), this,CANT_PEATONES3);
        nums[3]=new celdaNum(39, 6, 0, icons[0].getImage(), this,CANT_PEATONES4);
        nums[4]=new celdaNum(6, 15, 0, icons[0].getImage(), this,CANT_PEATONES5);
        nums[5]=new celdaNum(17, 15, 0, icons[0].getImage(), this,CANT_PEATONES6);
        nums[6]=new celdaNum(28, 15, 0, icons[0].getImage(), this,CANT_PEATONES7);
        nums[7]=new celdaNum(39, 15, 0, icons[0].getImage(), this,CANT_PEATONES8);

        autos = new auto[CANT_AUTOS];
        for (int i = 0; i < autos.length; i++) {
            autos[i] = new auto('l', icons[15].getImage(), icons[10].getImage(), icons[3].getImage(),
                    canvas_padre.miRandom(0, 20), 20, 3, icons[9].getImage(), this);
            if (i + 1 < autos.length) {
                i++;
                autos[i] = new auto('l', icons[15].getImage(), icons[10].getImage(), icons[3].getImage(),
                        canvas_padre.miRandom(0, 12), 20, 3, icons[9].getImage(), this);
            }
            if (i + 2 < autos.length) {
                i++;
                autos[i] = new auto('l', icons[15].getImage(), icons[10].getImage(), icons[3].getImage(),
                        canvas_padre.miRandom(22, 34), 20, 3, icons[9].getImage(), this);
            }
            if (i + 3 < autos.length) {
                i++;
                autos[i] = new auto('l', icons[15].getImage(), icons[10].getImage(), icons[3].getImage(),
                        canvas_padre.miRandom(34, 45), 20, 3, icons[9].getImage(), this);
            }
        }
        peatones=new peaton[CANT_PEATONES];

        for (int i = 0; i < peatones.length; i++) {
            peatones[i] = new peaton(canvas_padre.miRandom(2, 10), 18, 4, icons[4].getImage(), this, icons[17].getImage());
            if (i + 1 < peatones.length) {
                i++;
                 peatones[i]=new peaton(canvas_padre.miRandom(13, 21), 18, 4, icons[4].getImage(), this, icons[17].getImage());
            }
            if (i + 2 < peatones.length) {
                i++;
                 peatones[i]=new peaton(canvas_padre.miRandom(24, 32), 18, 4, icons[4].getImage(), this, icons[17].getImage());
            }
            if (i + 3 < peatones.length) {
                i++;
                 peatones[i]=new peaton(canvas_padre.miRandom(35, 43), 18, 4, icons[4].getImage(), this, icons[17].getImage());
            }
        }
        cartero = new cartero(1, 0, 2, icons[2].getImage(), this, new ImageIcon("include\\c.png").getImage(), CANT_CARTAS);
        bus = new bus(44, 19, 12, icons[12].getImage(), this, icons[14].getImage(), CANT_PASAJEROS, icons[16].getImage());
        mensaje = new mensaje(15, 9, 20, null, this);
        
        ladron = new ladron(canvas_padre.miRandom(2, 40), 18, 18, icons[18].getImage(), this, icons[19].getImage());
    }

    final String leer(String nombre) {
        File f = new File(nombre);
        StringBuilder salida = new StringBuilder();
        try {
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            String aux = "";
            aux = br.readLine();
            while (aux != null) {
                salida.append(aux + "\n");
                aux = br.readLine();
            }
            br.close();
            fr.close();
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
        return salida.toString();
    }

    public Image createResizedCopy(Image originalImage, int scaledWidth, int scaledHeight) {
        BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = scaledBI.createGraphics();
        g.setComposite(AlphaComposite.Src);

        g = scaledBI.createGraphics();
        g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, null);
        g.dispose();
        originalImage = scaledBI;
        return originalImage;
    }

    final void cargarImages() {
        for (int i = 0; i < CANT_SPRITES; i++) {
            icons[i] = new ImageIcon("include\\" + i + ".png");
            icons[i].setImage(createResizedCopy(icons[i].getImage(), DIM_CELDA, DIM_CELDA));
        }
    }

    @Override
    public void update(Graphics g) {
        for (int j = 0; j < CELDAS_ALTO; j++) {
            for (int i = 0; i < CELDAS_ANCHO; i++) {
                celdas[i][j].update(g);
            }
        }
        for (int i = 0; i < autos.length; i++) {
            autos[i].update(g);
        }
        for (int i = 0; i < peatones.length; i++) {
            peatones[i].update(g);
        }
        cartero.update(g);
        bus.update(g);
        mensaje.update(g);
        for (int i = 0; i < nums.length; i++) {
            nums[i].update(g);
        }
        ladron.update(g);
    }

    @Override
    public void paintComponent(Graphics g) {
        update(g);
    }
}
