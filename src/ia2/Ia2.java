/*
 * To change this license header, choose License Headers in Project Properties.
 */
package ia2;

import javax.swing.JFrame;

/**
 *
 * @author Juan Cid <jucid@alumnos.ubiobio.cl>
 */
public class Ia2 implements ctes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame jframe = new JFrame("Ciudad");
        jframe.setVisible(true);
        jframe.setDefaultCloseOperation(3);

        mi_canvas canvas = new mi_canvas();
        jframe.add(canvas);

        canvas.requestFocusInWindow();
        jframe.setSize(ANCHO_V + 20 + (CELDAS_ANCHO), ALTO_V + 45 + (CELDAS_ALTO));
    }

}
