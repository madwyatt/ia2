/*
 * To change this license header, choose License Headers in Project Properties.
 */
package ia2;

import java.awt.*;
import javax.swing.JComponent;

/**
 *
 * @author Juan Cid <jucid@alumnos.ubiobio.cl>
 */
public class celda extends JComponent implements ctes {

    protected int j;
    protected int x;
    protected int y;
    protected int i;
    double cant_peatones;
    protected int tipo;
    protected Image icon;
    protected ciudad ciudad;

    public celda(int x, int y, int i, int j, int tipo, Image icon, ciudad ciudad) {
        this.x = x;
        this.y = y;
        this.i = i;
        this.j = j;
        this.tipo = tipo;
        this.icon = icon;
        this.ciudad = ciudad;
        this.cant_peatones=0;
    }

    @Override
    public void paintComponent(Graphics g) {
        update(g);
    }

    @Override
    public void update(Graphics g) {
        g.setColor(Color.cyan);
        g.drawRect(x, y, DIM_CELDA, DIM_CELDA);
        g.drawImage(icon, x, y, this);
        
//        g.setColor(Color.red);
//        g.setFont(new Font("Monospaced", Font.PLAIN, 10));
//        g.drawString(i + "," + j, x + 1, y + 20);
 //       g.drawString(tipo + "", x + 1, y + 20);
   // g.drawString(cant_peatones+"", x+1, y+20);
    }

    public void cambiar_celda(int clickX, int clickY) {
        Rectangle rectanguloCelda = new Rectangle(x, y, DIM_CELDA, DIM_CELDA);
        if (rectanguloCelda.contains(new Point(clickX, clickY))) {
            if (tipo == 0) {
                tipo = 5;
                this.icon = ciudad.icons[5].getImage();
            } else {
                tipo = 0;
                this.icon = ciudad.icons[0].getImage();
            }
        }
    }

}
