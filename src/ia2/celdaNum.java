/*
 * To change this license header, choose License Headers in Project Properties.
 */
package ia2;

import static ia2.ctes.DIM_CELDA;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

/**
 *
 * @author Juan Cid <jucid@alumnos.ubiobio.cl>
 */
public class celdaNum extends celda implements ctes {

    public celdaNum(int i, int j, int tipo, Image icon, ia2.ciudad ciudad, int cant_p) {
        super(ciudad.celdas[i][j].x, ciudad.celdas[i][j].y, i, j, tipo, icon, ciudad);
        this.cant_peatones = cant_p;
    }

    @Override
    public void update(Graphics g) {
        //g.setColor(Color.cyan);
        //g.drawRect(x, y, DIM_CELDA, DIM_CELDA);
        g.drawImage(icon, x, y, this);
        g.setColor(Color.BLUE);
        g.setFont(new Font("Monospaced", Font.PLAIN, 12));
        g.drawString((int)cant_peatones + "", x + 1, y + 20);
        //       g.drawString(tipo + "", x + 1, y + 25);
    }
}
