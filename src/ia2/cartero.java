/*
 * To change this license header, choose License Headers in Project Properties.
 */
package ia2;

import java.awt.Graphics;
import java.awt.Image;

/**
 *
 * @author Juan Cid <jucid@alumnos.ubiobio.cl>
 */
public class cartero extends celda implements ctes {

    Image carta;
    int cartas, tipoAnterior;

    public cartero(int i, int j, int tipo, Image icon, ciudad ciudad, Image carta, int cartas) {
        super(ciudad.celdas[i][j].x, ciudad.celdas[i][j].y, i, j, tipo, icon, ciudad);
        this.carta = carta;
        this.cartas = cartas;
        this.tipoAnterior = ciudad.celdas[i][j].tipo;
        ciudad.celdas[i][j].tipo = tipo;
    }

    @Override
    public void update(Graphics g) {
        g.drawImage(icon, x, y, this);
        for (int k = 0; k < cartas; k++) {
            g.drawImage(carta, x, y + k * 5, this);
            if (k + 1 < cartas) {
                g.drawImage(carta, x + 17, y + k * 5, this);
                k++;
            }
        }
    }

    protected void arriba() {
        if (j > 0) {
            if (puedo_pizar(0, -1)) {
                avanzar('u');
            }
        }
    }

    protected void abajo() {
        if (j + 1 < CELDAS_ALTO) {
            if (puedo_pizar(0, 1)) {
                avanzar('d');
            }
        }
    }

    public void izq() {
        if (i > 0) {
            if (puedo_pizar(-1, 0)) {
                avanzar('l');
            }
        }
    }

    protected void der() {
        if (i + 1 < CELDAS_ANCHO) {
            if (puedo_pizar(1, 0)) {
                avanzar('r');
            }
        }
    }

    public boolean puedo_pizar(int x, int y) {
        return ciudad.celdas[i + x][j + y].tipo != 5 && ciudad.celdas[i + x][j + y].tipo != 3
                && ciudad.celdas[i + x][j + y].tipo != 4 && ciudad.celdas[i + x][j + y].tipo != 12;
    }

    public void avanzar(char mov) {
        ciudad.celdas[i][j].tipo = tipoAnterior;
        int suma = DIM_CELDA + 1;
        switch (mov) {
            case 'd':
                j++;
                y += suma;
                break;
            case 'u':
                j--;
                y -= suma;
                break;
            case 'l':
                i--;
                x -= suma;
                break;
            case 'r':
                i++;
                x += suma;
                break;
        }
        this.tipoAnterior = ciudad.celdas[i][j].tipo;
        if (ciudad.celdas[i][j].tipo != 11) {
            ciudad.celdas[i][j].tipo = tipo;
        }
    }
}
