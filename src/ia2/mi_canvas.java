/*
 * To change this license header, choose License Headers in Project Properties.
 */
package ia2;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Timer;

/**
 *
 * @author Juan Cid <jucid@alumnos.ubiobio.cl>
 */
public class mi_canvas extends Canvas implements ctes {

    public ciudad ciudad;
    Graphics gBuff;
    Image iBuff;
    Timer timer;
    move move;

    public mi_canvas() {
        ciudad = new ciudad(this);
        this.setBackground(Color.ORANGE);
        timer = new Timer();
        move = new move(this);

        for (int i = 0; i < CELDAS_ANCHO; i++) {
            for (int j = 0; j < CELDAS_ALTO; j++) {
                if (ciudad.celdas[i][j].tipo == 11) {
                    move.destinos.add(new estado(i, j, 'N', null));
                    //move.destinos.add(new estado(0, 0, 'N', null));
                }
            }
        }
        timer.scheduleAtFixedRate(move, 500, 250);
      

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if (ciudad.mensaje.cerrar(me.getX(), me.getY())) {
                    move.msg = false;
                } else {
                    cambiaCelda(me);
                    update(gBuff);
                }
            }
        });
    }

    @Override
    public void update(Graphics g) {
        if (gBuff == null) {
            iBuff = createImage(this.getWidth(), this.getHeight());
            gBuff = iBuff.getGraphics();
        }
        gBuff.setColor(getBackground());
        gBuff.fillRect(0, 0, this.getWidth(), this.getHeight());

        g.drawImage(iBuff, 0, 0, this);
        ciudad.update(g);
    }

    @Override
    public void paint(Graphics g) {
        update(g);
    }

    void cambiaCelda(MouseEvent event) {
        for (int i = 0; i < CELDAS_ANCHO; i++) {
            for (int j = 0; j < CELDAS_ALTO; j++) {
                ciudad.celdas[i][j].cambiar_celda(event.getX(), event.getY());
            }
        }
    }

    public int miRandom(int min, int max) {
        return (int) Math.floor(Math.random() * (max - min + 1) + min);
    }
}
