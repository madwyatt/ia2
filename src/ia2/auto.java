/*
 * To change this license header, choose License Headers in Project Properties.
 */
package ia2;

import java.awt.Graphics;
import java.awt.Image;

/**
 *
 * @author Juan Cid <jucid@alumnos.ubiobio.cl>
 */
public class auto extends celda implements ctes {

    char sentido;
    Image up, down, rigth, left;
    int tipoAnterior;

    public auto(char sentido, Image up, Image down, Image left, int i, int j, int tipo, Image icon, ciudad ciudad) {
        super(ciudad.celdas[i][j].x, ciudad.celdas[i][j].y, i, j, tipo, icon, ciudad);
        this.up = up;
        this.down = down;
        this.rigth = icon;
        this.left = left;
        this.tipoAnterior = ciudad.celdas[i][j].tipo;
        ciudad.celdas[i][j].tipo = tipo;
    }

    @Override
    public void update(Graphics g) {
        g.drawImage(icon, x, y, this);
    }

    protected void arriba() {
        if (j > 0) {
            if (puedo_pizar(0, -1)) {
                if (sentido != 'u') {
                    this.icon = up;
                    this.sentido = 'u';
                }
                avanzar('u');
            }
        }
    }

    protected void abajo() {
        if (j + 1 < CELDAS_ALTO) {
            if (puedo_pizar(0, 1)) {
                if (sentido != 'd') {
                    this.icon = down;
                    this.sentido = 'd';
                }
                avanzar('d');
            }
        }
    }

    public void izq() {
        if (i > 0) {
            if (puedo_pizar(-1, 0)) {
                if (sentido != 'l') {
                    this.icon = left;
                    this.sentido = 'l';
                }
                avanzar('l');
            }
        }
    }

    protected void der() {
        if (i + 1 < CELDAS_ANCHO) {
            if (puedo_pizar(1, 0)) {
                if (sentido != 'r') {
                    this.icon = rigth;
                    this.sentido = 'r';
                }
                avanzar('r');
            }
        }
    }

    public boolean puedo_pizar(int x, int y) {
        return ciudad.celdas[i + x][j + y].tipo == 1 || ciudad.celdas[i + x][j + y].tipo == 7
                || ciudad.celdas[i + x][j + y].tipo == 8;
    }

    public void avanzar(char mov) {
        ciudad.celdas[i][j].tipo = tipoAnterior;
        int suma = DIM_CELDA + 1;
        switch (mov) {
            case 'd':
                j++;
                y += suma;
                break;
            case 'u':
                j--;
                y -= suma;
                break;
            case 'l':
                i--;
                x -= suma;
                break;
            case 'r':
                i++;
                x += suma;
                break;
        }
        this.tipoAnterior = ciudad.celdas[i][j].tipo;
        ciudad.celdas[i][j].tipo=tipo;
    }

}
