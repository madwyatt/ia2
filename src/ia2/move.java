/*
 * To change this license header, choose License Headers in Project Properties.
 */
package ia2;

import java.util.ArrayList;
import java.util.TimerTask;

/**
 *
 * @author Juan Cid <jucid@alumnos.ubiobio.cl>
 */
public class move extends TimerTask implements ctes {

    mi_canvas canvas;
    ciudad ciudad;
    boolean msg;
    boolean escape;

    public ArrayList<estado> colaEstados;
    public ArrayList<estado> historial;
    public ArrayList<Character> pasos;
    public ArrayList<estado> destinos;
    public int index_pasos;
    public estado inicial;
    public estado objetivo;
    public estado temp;
    public boolean exito;
    public boolean parar;

    public move(mi_canvas canvas) {
        this.canvas = canvas;
        this.ciudad = canvas.ciudad;
        msg = false;
        escape = true;

        colaEstados = new ArrayList<>();
        historial = new ArrayList<>();
        pasos = new ArrayList<>();
        destinos = new ArrayList<>();
        index_pasos = 0;
        exito = false;
        parar = false;
    }

    public boolean buscar(estado inicial, estado objetivo) {
        System.out.println("Estado inicial" + inicial.toString() + "-> " + "Estado objetivo" + objetivo.toString());
        index_pasos = 0;
        colaEstados.add(inicial);
        historial.add(inicial);
        this.objetivo = objetivo;
        exito = false;

        if (inicial.equals(objetivo)) {
            exito = true;
        }

        while (!colaEstados.isEmpty() && !exito) {
            temp = colaEstados.get(0);
            colaEstados.remove(0);
            moverArriba(temp);
            moverAbajo(temp);
            moverIzquierda(temp);
            moverDerecha(temp);
        }
        if (exito) {
            System.out.println("Ruta calculada");
            this.calcularRuta();
            return true;
        } else {
            System.out.println("La ruta no pudo calcularse");
            return false;
        }

    }

    private void moverArriba(estado e) {
        if (e.y > 0) {
            if (puedo_pizar(0, -1, e)) {
                estado arriba = new estado(e.x, e.y - 1, 'U', e);
                agregaEstado(arriba);
            }
        }
    }

    private void moverAbajo(estado e) {
        if (e.y + 1 < CELDAS_ALTO) {
            if (puedo_pizar(0, 1, e)) {
                estado abajo = new estado(e.x, e.y + 1, 'D', e);
                agregaEstado(abajo);
            }
        }
    }

    private void moverIzquierda(estado e) {
        if (e.x > 0) {
            if (puedo_pizar(-1, 0, e)) {
                estado izquierda = new estado(e.x - 1, e.y, 'L', e);
                agregaEstado(izquierda);
            }
        }
    }

    private void moverDerecha(estado e) {
        if (e.x + 1 < CELDAS_ANCHO) {
            if (puedo_pizar(1, 0, e)) {
                estado derecha = new estado(e.x + 1, e.y, 'R', e);
                agregaEstado(derecha);
            }
        }
    }

    public boolean puedo_pizar(int x, int y, estado e) {
        return ciudad.celdas[e.x + x][e.y + y].tipo != 5 && ciudad.celdas[e.x + x][e.y + y].tipo != 3 && ciudad.celdas[e.x + x][e.y + y].tipo != 4
                && ciudad.celdas[e.x + x][e.y + y].tipo != 12 && ciudad.celdas[e.x + x][e.y + y].tipo != 17 && ciudad.celdas[e.x + x][e.y + y].tipo != 18;
    }

    public void agregaEstado(estado e) {
        if (!historial.contains(e)) {
            colaEstados.add(e);
            historial.add(e);
            if (e.equals(objetivo)) {
                objetivo = e;
                exito = true;
            }
        }
    }

    public void calcularRuta() {
        estado predecesor = objetivo;
        do {
            pasos.add(0, predecesor.oper);
            predecesor = predecesor.predecesor;
        } while (predecesor != null);
        index_pasos = pasos.size() - 1;

    }

    public double distancia(int x1, int y1, int x2, int y2) {
        double valor;
        double parte1 = Math.pow(Math.abs(x1 - x2), 2);
        double parte2 = Math.pow(Math.abs(y1 - y2), 2);
        parte1 += parte2;
        valor = Math.sqrt(parte1);
        //System.out.println("valor: "+valor);
        return valor;
    }

    public void moverCartero() {
        if (distancia(ciudad.cartero.i, ciudad.cartero.j, ciudad.ladron.i, ciudad.ladron.j) < 4 && escape) {
            parar=true;
            escapeCartero(ciudad.cartero, ciudad.ladron);
        } else if (!parar) {
            colaEstados.clear();
            historial.clear();
            pasos.clear();

            estado subinicial, subobjetivo;
            boolean resultado;
            if (!destinos.isEmpty()) {
                do {
                    subinicial = new estado(ciudad.cartero.i, ciudad.cartero.j, 'N', null);
                    subobjetivo = destinos.get(0);
                    resultado = this.buscar(subinicial, subobjetivo);

                    if (subinicial.equals(subobjetivo)) {
                        destinos.remove(subobjetivo);
                    } else if (!resultado) {
                        colaEstados.clear();
                        historial.clear();
                        pasos.clear();
                        destinos.remove(subobjetivo);
                    }
                    if (destinos.isEmpty()) {
                        System.out.println("Se acabo a donde ir");
                        // this.cancel();
                    }
                } while (!resultado && !destinos.isEmpty());
            }

            if (pasos.size() > 1) {
                switch (pasos.get(1)) {
                    case 'D':
                        ciudad.cartero.abajo();
                        break;
                    case 'U':
                        ciudad.cartero.arriba();
                        break;
                    case 'R':
                        ciudad.cartero.der();
                        break;
                    case 'L':
                        ciudad.cartero.izq();
                        break;
                }
                if (ciudad.celdas[ciudad.cartero.i][ciudad.cartero.j].tipo == 11 && ciudad.cartero.cartas >= 0) {
                    ciudad.cartero.cartas--;
                }
                if (ciudad.celdas[ciudad.cartero.i][ciudad.cartero.j].tipo == 11) {
                    msg = true;
                }
                if (ciudad.cartero.cartas == 0) {//tipo inicio

                }
            }
        }
    }

    public void moverBus() {
        if (ciudad.bus.i > 1 && ciudad.bus.j == 19) {
            ciudad.bus.izq();
        } else if (ciudad.bus.i == 1 && ciudad.bus.j > 11) {
            ciudad.bus.arriba();
        } else if (ciudad.bus.i < 44 && ciudad.bus.j == 11) {
            ciudad.bus.der();
        } else {
            ciudad.bus.abajo();
        }
        if (ciudad.celdas[ciudad.bus.i][ciudad.bus.j - 1].tipo == 13 && ciudad.bus.pasajeros >= 0) {
            ciudad.bus.pasajeros--;
        }
    }

    public void rutaAuto(auto auto, int a, int b, int c, int d) {
        if (auto.i < a && auto.j == b) {
            auto.der();
        } else if (auto.i == a && auto.j > c) {
            auto.arriba();
        } else if (auto.i > d && auto.j == c) {
            auto.izq();
        } else {
            auto.abajo();
        }
    }

    public void rutaPeaton(peaton peaton, int a, int b, int c, int d) {
        if (peaton.i < a && peaton.j == b) {
            peaton.der();
        } else if (peaton.i == a && peaton.j > c) {
            peaton.arriba();
        } else if (peaton.i > d && peaton.j == c) {
            peaton.izq();
        } else {
            peaton.abajo();
        }
    }

    public void moverPeatones() {
        for (int i = 0; i < ciudad.peatones.length; i++) {
            rutaPeaton(ciudad.peatones[i], 10, 18, 3, 2);
            if (i + 1 < ciudad.peatones.length) {
                i++;
                rutaPeaton(ciudad.peatones[i], 21, 18, 3, 13);
            }
            if (i + 2 < ciudad.peatones.length) {
                i++;
                rutaPeaton(ciudad.peatones[i], 32, 18, 3, 24);
            }
            if (i + 3 < ciudad.peatones.length) {
                i++;
                rutaPeaton(ciudad.peatones[i], 43, 18, 3, 35);
            }
        }
    }

    public void moverAutos() {
        for (int i = 0; i < ciudad.autos.length; i++) {
            rutaAuto(ciudad.autos[i], 23, 20, 1, 0);
            if (i + 1 < ciudad.autos.length) {
                i++;
                rutaAuto(ciudad.autos[i], 12, 20, 1, 0);
            }
            if (i + 2 < ciudad.autos.length) {
                i++;
                rutaAuto(ciudad.autos[i], 34, 20, 1, 22);
            }
            if (i + 3 < ciudad.autos.length) {
                i++;
                rutaAuto(ciudad.autos[i], 45, 20, 1, 22);
            }
        }
    }

    @Override
    public void run() {
        if (!msg) {
            moverPeatones();
            moverAutos();
            moverBus();
            moverCartero();
            moverLadron(ciudad.ladron);
            ciudad.update(canvas.getGraphics());
        }
    }

    public boolean pisarLadron(int i, int j) {
        return ciudad.celdas[ciudad.ladron.i + i][ciudad.ladron.j + j].tipo != 5 && ciudad.celdas[ciudad.ladron.i + i][ciudad.ladron.j + j].tipo != 3 && ciudad.celdas[ciudad.ladron.i + i][ciudad.ladron.j + j].tipo != 4
                && ciudad.celdas[ciudad.ladron.i + i][ciudad.ladron.j + j].tipo != 12 && ciudad.celdas[ciudad.ladron.i + i][ciudad.ladron.j + j].tipo != 17 && ciudad.celdas[ciudad.ladron.i + i][ciudad.ladron.j + j].tipo != 11;
    }

    private void moverLadron(ladron ladron) {
        if (ciudad.cartero.i > ladron.i && pisarLadron(1, 0)) {
            ladron.der();
        } else if (ciudad.cartero.j > ladron.j && pisarLadron(0, 1)) {
            ladron.abajo();
        } else if (ciudad.cartero.i < ladron.i && pisarLadron(-1, 0)) {
            ladron.izq();
        } else if (pisarLadron(0, -1)) {
            ladron.arriba();
        }
        if (ciudad.cartero.i == ladron.i && ciudad.cartero.j == ladron.j) {
            ciudad.cartero.cartas = 0;
            ciudad.mensaje.mString = "Cartero Sin Cartas¡¡¡";
            msg = true;
        }
    }

    public void escapeCartero(cartero cartero, ladron ladron) {
        double[] a = new double[4];
        a[0] = distancia(cartero.i + 1, cartero.j, ladron.i, ladron.j);
        a[1] = distancia(cartero.i - 1, cartero.j, ladron.i, ladron.j);
        a[2] = distancia(cartero.i + 1, cartero.j + 1, ladron.i, ladron.j);
        a[3] = distancia(cartero.i + 1, cartero.j - 1, ladron.i, ladron.j);
        double te = 0;
        for (int i = 0; i < a.length; i++) {
            if (te < a[i]) {
                te = a[i];
            }
        }

        if (te == a[0]) {
            cartero.abajo();
        } else if (te == a[1]) {
            cartero.arriba();
        } else if (te == a[2]) {
            cartero.der();
        } else if (te == a[3]) {
            cartero.izq();
        }
    }
}
